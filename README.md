# tf_gcp_gitlab_project

**tf_gcp_gitlab_project** is a test project to deploy gitlab-runner on GCP 

## 0. Prerequisite
* GCP account
* gitlab account

## 1. Launch gitlab-runner on VM
* Create a terraform script to launch a vm. You have an example in maint.tf file
* Connect on Cloud Shell
* Create a repository : mkdir tf_gcp_gitlab_project
* cd tf_gcp_gitlab_project
* Import main.tf on Cloud Shell
* Run : terraform init
* Run : terraform plan
* Run : terraform apply
* Connect on the vm and run with a root user : gitlab-runner register (your credential are in setting | ci/cd | runner | Set up a specific Runner manually)

