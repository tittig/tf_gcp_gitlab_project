#provider information
provider "google" {
  project = "<your project>"
  region  = "europe-west1"
  zone    = "europe-west1-b"
}

provider "google-beta" {
  project = "<your project>"
  region  = "europe-west1"
  zone    = "europe-west1-b"
}

# vm_instance
resource "google_compute_instance" "vm_instance" {
  project = "<your project>"
  description = "vm to run gitlab runner"
  
  provider = "google-beta"
  name = "gitlab-runner-vm"
  zone = "europe-west1-b"
  machine_type = "e2-small"
  tags = ["https-server"]

  scheduling {
    preemptible = true
    automatic_restart = false
  }

  boot_disk {
    auto_delete = true
    
    initialize_params {
      image = "centos-7-v20200910"
      type="pd-standard"
      size = 20
    }
  }
  
  metadata = {
    startup-script-url = "https://storage.cloud.google.com/<your storage>/start_vm_gitlab-runner.sh"
  }

  service_account {
    scopes = [ 
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
  network_interface {
    # A default network is created for all GCP projects
    network = "default"
    access_config {
    }
  }
}